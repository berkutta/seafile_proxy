# Proxy for Seafile

This Dockerfile is used to redirect everything Seahub related to Port 8000 and the other 
Seafile related to Port 8082. Perfect addition to the [jenserat/seafile](https://hub.docker.com/r/jenserat/seafile/) image.

## Setup

Basic example:

    # Run Seafile Proxy
    docker run -d \
      -p 80:80 \
      --link SEAFILECONTAINER:seafile_server \
      berkutta/seafile_proxy


Advanced example with jwilder/nginx-proxy (needs an already setup Seafile):

    # Run nginx proxy
    docker run -d -p 80:80 -p 443:443 \
        --name nginx_proxy \
        -v /storage/certs:/etc/nginx/certs:ro \
        -v /etc/nginx/vhost.d \
        -v /usr/share/nginx/html \
        -v /var/run/docker.sock:/tmp/docker.sock:ro \
        jwilder/nginx-proxy
    # Run Seafile
    docker run -d \
        --name=seafile \
        -e autostart=true \
        -v /storage/seafile:/opt/seafile \
        jenserat/seafile
    # Run Seafile Proxy
    docker run -d \
        --name=seafile_proxy \
        --link seafile:seafile_server \
        -e "VIRTUAL_HOST=YOURHOST" \
        -e "LETSENCRYPT_HOST=YOURHOST" \
        -e "LETSENCRYPT_EMAIL=YOUREMAIL" \
        berkutta/seafile_proxy


## Usage

With this Seafile Proxy you're able to use the Seafile Container e.g. through the [jwilder/nginx-proxy](https://hub.docker.com/r/jwilder/nginx-proxy/) without any hassle.